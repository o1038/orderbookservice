package com.orderbook.orderbookservice.responsers;

import com.google.gson.Gson;

public class AbstractResponse {
	
	public String toJson() {
		return new Gson().toJson(this);
	}

}
