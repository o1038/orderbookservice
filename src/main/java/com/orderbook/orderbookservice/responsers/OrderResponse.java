package com.orderbook.orderbookservice.responsers;

import com.orderbook.orderbookservice.models.Order;

public class OrderResponse extends AbstractResponse{
	
	private Long id;
	private Long idWallet;
	private Long idMarket;
	private Double price;
	private Double amount;
	private String status;
	private Double traded;
	
	public OrderResponse(Order order) {
		this.id = order.getId();
		this.idWallet = order.getIdWallet();
		this.idMarket = order.getIdMarket();
		this.price = order.getPrice();
		this.amount = order.getAmount();
		this.status = order.getStatus();
		this.traded = order.getTraded();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdWallet() {
		return idWallet;
	}
	public void setIdWallet(Long idWallet) {
		this.idWallet = idWallet;
	}
	public Long getIdMarket() {
		return idMarket;
	}
	public void setIdMarket(Long idMarket) {
		this.idMarket = idMarket;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Double getTraded() {
		return traded;
	}
	public void setTraded(Double traded) {
		this.traded = traded;
	}
	
	

}
