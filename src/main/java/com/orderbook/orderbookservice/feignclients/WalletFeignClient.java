package com.orderbook.orderbookservice.feignclients;

import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.orderbook.orderbookservice.requests.TradeRequest;

@Component
@FeignClient(name = "wallet-service", path="/wallet")
@LoadBalancerClient(name = "wallet-service")
public interface WalletFeignClient {
	
	@PostMapping(value="/execute-orders")
	public ResponseEntity<?> execOrders(@RequestBody TradeRequest request);

}
