package com.orderbook.orderbookservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orderbook.orderbookservice.models.Market;

@Repository
public interface MarketRepository extends JpaRepository<Market, Long>{

	
}
