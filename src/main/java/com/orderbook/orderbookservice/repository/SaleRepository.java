package com.orderbook.orderbookservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orderbook.orderbookservice.models.Sale;

public interface SaleRepository extends JpaRepository<Sale, Long>{
	
	public List<Sale> findByIdMarketAndStatus(Long id, String Status);

}
