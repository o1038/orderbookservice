package com.orderbook.orderbookservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orderbook.orderbookservice.models.Trade;

@Repository
public interface TradeRepository extends JpaRepository<Trade, Long>{

}
