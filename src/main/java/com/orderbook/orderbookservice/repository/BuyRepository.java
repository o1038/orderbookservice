package com.orderbook.orderbookservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orderbook.orderbookservice.models.Buy;

public interface BuyRepository extends JpaRepository<Buy, Long>{

	
	public List<Buy> findByPriceAndStatus(Double price, String status);
	
}
