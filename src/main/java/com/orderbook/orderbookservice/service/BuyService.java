package com.orderbook.orderbookservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orderbook.orderbookservice.enums.StatusOrder;
import com.orderbook.orderbookservice.models.Buy;
import com.orderbook.orderbookservice.models.Market;
import com.orderbook.orderbookservice.repository.BuyRepository;
import com.orderbook.orderbookservice.repository.MarketRepository;
import com.orderbook.orderbookservice.requests.OrderRequest;

@Service
public class BuyService {
	
	@Autowired
	private BuyRepository buyRepository;
	@Autowired
	private MarketRepository marketRepository;
	
	public Buy createBuy(OrderRequest order) {
		
		Buy buy = new Buy(order);
		buy.setStatus(StatusOrder.PENDING.getStatus());
		Market market = marketRepository.getById(order.getIdWallet());
		buy.setIdMarket(market.getId());
		return insertBuy(buy);
		
	}
	
	public Buy insertBuy(Buy sale) {
		return buyRepository.save(sale);
	}
	
	public Buy save(Buy buy) {
		
		if(buy.getAvailable() <= 0) {
			buy.setStatus(StatusOrder.DEALDED.getStatus());
		}
		
		return buyRepository.save(buy);
	}
	
	public Buy findByPricePending(Double price) {
		
		List<Buy> buys = buyRepository.findByPriceAndStatus(price, StatusOrder.PENDING.getStatus());
		if(buys.isEmpty()) {
			return null;
		}
		return buys.get(0);
				
	}

}
