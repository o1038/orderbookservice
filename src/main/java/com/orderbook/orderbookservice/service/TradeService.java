package com.orderbook.orderbookservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.orderbook.orderbookservice.feignclients.WalletFeignClient;
import com.orderbook.orderbookservice.models.Buy;
import com.orderbook.orderbookservice.models.Market;
import com.orderbook.orderbookservice.models.Sale;
import com.orderbook.orderbookservice.models.Trade;
import com.orderbook.orderbookservice.repository.MarketRepository;
import com.orderbook.orderbookservice.repository.TradeRepository;
import com.orderbook.orderbookservice.requests.TradeRequest;

@Service
public class TradeService {

	
	@Autowired
	private SaleService saleService;
	@Autowired
	private BuyService buyService;
	@Autowired
	private TradeRepository tradeRepository;
	@Autowired
	private MarketRepository marketRepository;
	@Autowired
	private WalletFeignClient walletClient;
	
	@Scheduled(fixedRate = 10000)
	public void executeMarkets() {
		
		List<Market> markets = marketRepository.findAll();
		
		for(Market market : markets) {
			executeMarket(market.getId());
		}
	}
	
	@Transactional
	public void executeMarket(Long idMarket) {
		
		List<Sale> salesPending = saleService.findSalePending(idMarket);
		
		for(Sale sale : salesPending) {
			Buy buy = buyService.findByPricePending(sale.getPrice());
			if(buy != null) {
				Double amountTrade = 0.0;
				if(buy.getAvailable().compareTo(sale.getAvailable()) <= 0){
					amountTrade = buy.getAvailable();
				}else {
					amountTrade = sale.getAvailable();
				}
				executeTrade(sale, buy, amountTrade);
			}
		}
		
	}
	
	
	@Transactional
	public void executeTrade(Sale sale, Buy buy, Double amount) {
		
		Double traded = sale.getTraded();
		sale.setTraded(traded + amount);
		buy.setTraded(buy.getTraded()+ amount);
		
		saleService.save(sale);
		buyService.save(buy);
		
		Optional<Market> market = marketRepository.findById(sale.getIdMarket());
		createTrade(market.get(), sale, buy, amount);
		sendTrade(market.get(), sale, buy, amount);
		
	}
	
	public Trade createTrade(Market market,Sale sale, Buy buy, Double amountTraded) {
		return tradeRepository.save(new Trade(market, sale, buy, amountTraded));
	}
	
	public void sendTrade(Market market,Sale sale, Buy buy, Double amountTraded) {
		
		TradeRequest request = new TradeRequest();
		request.setIdWallet(sale.getIdWallet());
		request.setIdWalletTrade(buy.getIdWallet());
		request.setAmountPrincipal(amountTraded);
		request.setAmountSecond(amountTraded*buy.getPrice());
		request.setIdCoinPrincipal(market.getIdCoinPrincipal());
		request.setIdCoinSecond(market.getIdCoinSecond());
		
		walletClient.execOrders(request);
	}
	
	
}
