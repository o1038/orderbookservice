package com.orderbook.orderbookservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orderbook.orderbookservice.enums.StatusOrder;
import com.orderbook.orderbookservice.models.Market;
import com.orderbook.orderbookservice.models.Sale;
import com.orderbook.orderbookservice.repository.MarketRepository;
import com.orderbook.orderbookservice.repository.SaleRepository;
import com.orderbook.orderbookservice.requests.OrderRequest;

@Service
public class SaleService {
	
	@Autowired
	private SaleRepository saleRepository;
	@Autowired
	private MarketRepository marketRepository;
	
	
	public Sale createSale(OrderRequest order) {
		
		Sale sale = new Sale(order);
		sale.setStatus(StatusOrder.PENDING.getStatus());
		Market market = marketRepository.getById(order.getIdMarket());
		sale.setIdMarket(market.getId());
		
		return insertSale(sale);
		
	}
	
	public Sale insertSale(Sale sale) {
		return saleRepository.save(sale);
	}
	
	public List<Sale> findSalePending(Long idMarket) {
		return findSaleByMarketAndStatus(idMarket, StatusOrder.PENDING.getStatus());
	}
	
	public List<Sale> findSaleByMarketAndStatus(Long idMarket, String status) {
		
		return saleRepository.findByIdMarketAndStatus(idMarket, status);
		
	}
	
	public Sale save(Sale sale) {
		
		if(sale.getAvailable() <= 0) {
			sale.setStatus(StatusOrder.DEALDED.getStatus());
		}
		
		return saleRepository.save(sale);
	}

}
