package com.orderbook.orderbookservice;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableFeignClients
@SpringBootApplication
@EnableScheduling
@EnableRabbit
public class OrderBookServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderBookServiceApplication.class, args);
	}

}
