package com.orderbook.orderbookservice.models;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.orderbook.orderbookservice.requests.OrderRequest;

@Entity
@Table(name = "tb_buy")
public class Buy extends Order{

	public Buy(OrderRequest order) {
		super(order);
	}
	
	public Buy() {
	}

}
