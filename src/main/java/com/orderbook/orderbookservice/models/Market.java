package com.orderbook.orderbookservice.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_market")
public class Market {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "id_coin_p")
	private Long idCoinPrincipal;
	
	@Column(name = "id_coin_s")
	private Long idCoinSecond;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdCoinPrincipal() {
		return idCoinPrincipal;
	}

	public void setIdCoinPrincipal(Long idCoinPrincipal) {
		this.idCoinPrincipal = idCoinPrincipal;
	}

	public Long getIdCoinSecond() {
		return idCoinSecond;
	}

	public void setIdCoinSecond(Long idCoinSecond) {
		this.idCoinSecond = idCoinSecond;
	}
	
	
}
