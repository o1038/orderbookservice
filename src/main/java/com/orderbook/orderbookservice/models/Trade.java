package com.orderbook.orderbookservice.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tb_trade")
public class Trade {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "id_sale")
	private Sale sale;
	
	@ManyToOne
	@JoinColumn(name = "id_buy")
	private Buy buy;
	
	@ManyToOne
	@JoinColumn(name = "id_Market")
	private Market market;
	
	@Column(name = "amountTraded")
	private Double amountTraded;
	
	public Trade(Market market,Sale sale, Buy buy, Double amountTraded) {
		super();
		this.sale = sale;
		this.buy = buy;
		this.market = market;
		this.amountTraded = amountTraded;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Sale getSale() {
		return sale;
	}

	public void setSale(Sale sale) {
		this.sale = sale;
	}

	public Buy getBuy() {
		return buy;
	}

	public void setBuy(Buy buy) {
		this.buy = buy;
	}

	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		this.market = market;
	}
	
	
	
	
	
}
