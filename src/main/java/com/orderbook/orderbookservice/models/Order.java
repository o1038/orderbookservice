package com.orderbook.orderbookservice.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.orderbook.orderbookservice.requests.OrderRequest;

@MappedSuperclass
public abstract class Order {
	
	
	
	public Order() {
		super();
	}

	public Order(OrderRequest order) {
		this.idWallet = order.getIdWallet();
		this.id = order.getIdMarket();
		this.price = order.getPrice();
		this.amount = order.getAmount();
		this.idMarket = order.getIdMarket();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "idWallet")
	private Long idWallet;
	
	@Column(name = "price")
	private Double price;
	
	@Column(name = "amount")
	private Double amount;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "traded")
	private Double traded = 0.0;
	
	@Column(name = "idMarket")
	private Long idMarket;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdWallet() {
		return idWallet;
	}

	public void setIdWallet(Long idWallet) {
		this.idWallet = idWallet;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getTraded() {
		return traded;
	}

	public void setTraded(Double traded) {
		this.traded = traded;
	}

	public Long getIdMarket() {
		return idMarket;
	}

	public void setIdMarket(Long idMarket) {
		this.idMarket = idMarket;
	}
	public Double getAvailable() {
		return amount-traded;
	}
	
	
}
