package com.orderbook.orderbookservice.models;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.orderbook.orderbookservice.requests.OrderRequest;

@Entity
@Table(name="tb_sale")
public class Sale extends Order{

	public Sale(OrderRequest order) {
		super(order);
	}
	
	public Sale() {
		super();
	}
	

}
