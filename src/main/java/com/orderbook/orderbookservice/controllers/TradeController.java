package com.orderbook.orderbookservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orderbook.orderbookservice.service.TradeService;

@Controller
@RequestMapping(value = "/trade", produces = MediaType.APPLICATION_JSON_VALUE)
public class TradeController {
	
	@Autowired
	private TradeService tradeService;
	
	@PostMapping(value = "/execute-market/{id}")
	public BodyBuilder executeMarket(@PathVariable(name = "id") Long id){
		
			tradeService.executeMarket(id);
		
		return ResponseEntity.ok();
	}

}
