package com.orderbook.orderbookservice.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orderbook.orderbookservice.models.Sale;
import com.orderbook.orderbookservice.requests.OrderRequest;
import com.orderbook.orderbookservice.responsers.OrderResponse;
import com.orderbook.orderbookservice.service.SaleService;

@RestController
@RequestMapping(value = "/sale", produces = MediaType.APPLICATION_JSON_VALUE)
public class SaleController {
	
	
	@Autowired
	private SaleService saleService;

	@Autowired
	private Environment env;
	
	private static Logger logger = LoggerFactory.getLogger(SaleController.class);
	
	@PostMapping(value="/create")
	public ResponseEntity<?> createSale(@RequestBody OrderRequest order) {
		
		logger.info("========> Sale Controller PORT: " + env.getProperty("local.server.port"));
		
		Sale s = saleService.createSale(order);
				
		return ResponseEntity.ok(new OrderResponse(s));
		
	}

}
