package com.orderbook.orderbookservice.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orderbook.orderbookservice.models.Buy;
import com.orderbook.orderbookservice.requests.OrderRequest;
import com.orderbook.orderbookservice.responsers.OrderResponse;
import com.orderbook.orderbookservice.service.BuyService;

@RestController
@RequestMapping(value = "/buy", produces = MediaType.APPLICATION_JSON_VALUE)
public class BuyController {
	
	
	@Autowired
	private BuyService buyService;

	@Autowired
	private Environment env;
	
	private static Logger logger = LoggerFactory.getLogger(BuyController.class);
	
	@PostMapping(value="/create")
	public ResponseEntity<?> createBuy(@RequestBody OrderRequest order) {
		
		logger.info("========> Sale Controller PORT: " + env.getProperty("local.server.port"));
		
		Buy buy = buyService.createBuy(order);
				
		return ResponseEntity.ok(new OrderResponse(buy));
		
	}

}
