package com.orderbook.orderbookservice.rabbitmq;

import java.nio.charset.StandardCharsets;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.orderbook.orderbookservice.requests.OrderRequest;
import com.orderbook.orderbookservice.service.BuyService;
import com.orderbook.orderbookservice.service.SaleService;

@Component
public class QueueConsumer {

	private static final String SALE = "sale";
	private static final String BUY = "buy";

	@Autowired
	private SaleService saleService;
	
	@Autowired
	private BuyService buyService;
	
	@RabbitListener(queues = {"${queue.name}"})
    public void receive(@Payload Message message) {
	 
		 String tipo = String.valueOf( message.getHeaders().get("tipo"));
		 String payload = new String((byte[]) message.getPayload(), StandardCharsets.US_ASCII);
		 OrderRequest order = new Gson().fromJson(payload, OrderRequest.class);
		 
		 if(SALE.equals(tipo)) {
			 saleService.createSale(order);
		 }else if (BUY.equals(tipo)) {
			 buyService.createBuy(order);
		 }
	}
	
}
