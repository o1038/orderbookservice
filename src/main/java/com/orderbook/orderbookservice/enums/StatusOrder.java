package com.orderbook.orderbookservice.enums;

public enum StatusOrder {
	PENDING("P"), CANCELED("C"), DEALDED("D");

	private String status;
	
	StatusOrder(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
	
}
