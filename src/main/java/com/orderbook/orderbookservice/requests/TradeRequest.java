package com.orderbook.orderbookservice.requests;

public class TradeRequest {

	private Long idWallet;
	private Long idCoinPrincipal;
	private Double amountPrincipal;
	private Long idCoinSecond;
	private Double amountSecond;
	private Long idWalletTrade;

	public Long getIdWalletTrade() {
		return idWalletTrade;
	}
	public void setIdWalletTrade(Long idWalletTrade) {
		this.idWalletTrade = idWalletTrade;
	}
	public Long getIdWallet() {
		return idWallet;
	}
	public void setIdWallet(Long idWallet) {
		this.idWallet = idWallet;
	}
	public Long getIdCoinPrincipal() {
		return idCoinPrincipal;
	}
	public void setIdCoinPrincipal(Long idCoinPrincipal) {
		this.idCoinPrincipal = idCoinPrincipal;
	}
	public Double getAmountPrincipal() {
		return amountPrincipal;
	}
	public void setAmountPrincipal(Double amountPrincipal) {
		this.amountPrincipal = amountPrincipal;
	}
	public Long getIdCoinSecond() {
		return idCoinSecond;
	}
	public void setIdCoinSecond(Long idCoinSecond) {
		this.idCoinSecond = idCoinSecond;
	}
	public Double getAmountSecond() {
		return amountSecond;
	}
	public void setAmountSecond(Double amountSecond) {
		this.amountSecond = amountSecond;
	}
	
	
}
